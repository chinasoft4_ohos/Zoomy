/*
 * Copyright (C) 2021 Huawei Device Co., Ltd. 2012-2020. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.ablanco.zoomysample;

import com.ablanco.zoomy.Zoomy;
import ohos.agp.animation.Animator;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;

import java.util.List;

/**
 * recyclerView列表的provider
 *
 * @since 2021-07-07
 */
public class ImageProvider extends BaseItemProvider {
    private final Context context;
    private final List<Integer> imgs;

    private static final int TOAST_SHOW_DURATION = 2 * 1000;
    private static final int TOAST_SHOW_Y = 200;

    /**
     * 构造函数
     *
     * @param context 上下文
     * @param imgs    列表数据
     */
    public ImageProvider(Context context, List<Integer> imgs) {
        this.context = context;
        this.imgs = imgs;
    }

    @Override
    public int getCount() {
        if (imgs == null) {
            return 0;
        } else {
            int num = (imgs.size() % 2);
            int count = imgs.size() / 2;
            if (num != 0) {
                return (count + 1);
            }
            return (count);
        }
    }

    @Override
    public Object getItem(int position) {
        if (imgs != null && position >= 0 && position < imgs.size()) {
            return imgs.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int id) {
        return id;
    }

    @Override
    public Component getComponent(int position, Component convertComponent, ComponentContainer componentContainer) {

        final Component cpt;
        if (convertComponent == null) {
            cpt = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_item_img, null, false);
        } else {
            cpt = convertComponent;
        }
        SquareImageView image = (SquareImageView) cpt.findComponentById(ResourceTable.Id_img);

        SquareImageView image2 = (SquareImageView) cpt.findComponentById(ResourceTable.Id_img2);

        image.setTag(position * 2);
        image2.setTag(position * 2 + 1);
        image.setPixelMap(imgs.get(position * 2));
        image2.setPixelMap(imgs.get(position * 2 + 1));
        zoomyRegister(componentContainer, image);
        zoomyRegister(componentContainer, image2);
        return cpt;

    }

    private void zoomyRegister(ComponentContainer componentContainer, SquareImageView squareImageView) {

        Zoomy.Builder builder = new Zoomy.Builder((ComponentContainer) componentContainer.getComponentParent())
            .target(squareImageView)
            .interpolator(Animator.CurveType.OVERSHOOT)
            .tapListener(component -> {
                showToast("Tap on " + component.getTag());
            })
            .longPressListener(component -> {
                showToast("Long press on " + component.getTag());
            }).doubleTapListener(component -> {
                showToast("Double tap on " + component.getTag());
            });
        builder.register();
    }

    private void showToast(String toastMessage) {
        DirectionalLayout toastLayout = (DirectionalLayout) LayoutScatter.getInstance(context)
            .parse(ResourceTable.Layout_layout_toast, null, false);
        Text text = (Text) toastLayout.findComponentById(ResourceTable.Id_msg_toast);
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setCornerRadius(80);
        shapeElement.setRgbColor(new RgbColor(120, 120, 128, 50));
        text.setBackground(shapeElement);
        text.setText(toastMessage);
        new ToastDialog(context)
            .setContentCustomComponent(toastLayout)
            .setCornerRadius(80)
            .setSize(DirectionalLayout.LayoutConfig.MATCH_CONTENT, DirectionalLayout.LayoutConfig.MATCH_CONTENT)
            .setAlignment(LayoutAlignment.BOTTOM)
            .setDuration(TOAST_SHOW_DURATION)
            .setOffset(0, TOAST_SHOW_Y)
            .show();

    }

}
