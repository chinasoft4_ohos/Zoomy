/*
 * Copyright (C) 2021 Huawei Device Co., Ltd. 2012-2020. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.ablanco.zoomysample.slice;

import com.ablanco.zoomysample.ImageProvider;
import com.ablanco.zoomysample.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ListContainer;
import ohos.agp.utils.Color;
import ohos.agp.window.service.Window;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * MainAbilitySlice
 *
 * @since 2021-07-14
 */
public class MainAbilitySlice extends AbilitySlice {
    private final List<Integer> images = new ArrayList<>();

    @Override
    public void onStart(Intent intent) {
        setStatusBarColor();
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        ListContainer listContainer1 = (ListContainer) findComponentById(ResourceTable.Id_list);
        images.addAll(Arrays.asList(ResourceTable.Media_img1, ResourceTable.Media_img2,
            ResourceTable.Media_img3, ResourceTable.Media_img4, ResourceTable.Media_img5, ResourceTable.Media_img6,
            ResourceTable.Media_img7, ResourceTable.Media_img8, ResourceTable.Media_img9, ResourceTable.Media_img10));
        ImageProvider imageProvider = new ImageProvider(this, images);
        listContainer1.setItemProvider(imageProvider);
        listContainer1.setBoundarySwitch(true);
        listContainer1.setBoundaryThickness(5);
        listContainer1.setBindStateChangedListener(new Component.BindStateChangedListener() {
            @Override
            public void onComponentBoundToWindow(Component component) {
                imageProvider.notifyDataChanged();
            }

            @Override
            public void onComponentUnboundFromWindow(Component component) {}
        });
        listContainer1.setItemLongClickedListener((listContainer, component, position, id) -> {
            component.setEnabled(false);
            return false;
        });
    }

    /**
     * 设置状态栏颜色
     *
     * @since 2021-07-07
     */
    private void setStatusBarColor() {
        Window window = getWindow();
        window.setStatusBarColor(Color.rgb(63, 81, 181));
        window.setStatusBarVisibility(Component.VISIBLE);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }


}
