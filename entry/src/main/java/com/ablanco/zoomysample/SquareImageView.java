package com.ablanco.zoomysample;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.app.Context;

/**
 * Created by Álvaro Blanco Cabrero on 12/02/2017.
 * Zoomy.
 */
public class SquareImageView extends Image implements Component.EstimateSizeListener {

    /**
     * SquareImageView 构造方法
     *
     * @param context context
     * @since 2021-07-14
     */
    public SquareImageView(Context context) {
        this(context, null);
    }

    /**
     * SquareImageView 构造方法
     *
     * @param context context
     * @param attr attr
     * @since 2021-07-14
     */
    public SquareImageView(Context context, AttrSet attr) {
        this(context, attr, "");
    }

    /**
     * SquareImageView 构造方法
     *
     * @param context context
     * @param attrs attr
     * @param defStyleAttr defStyleAttr
     * @since 2021-07-14
     */
    public SquareImageView(Context context, AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setEstimateSizeListener(this);
    }


    @Override
    public boolean onEstimateSize(int widthEstimateConfig, int heightEstimateConfig) {
        int width = EstimateSpec.getSize(widthEstimateConfig);
        int height = EstimateSpec.getSize(widthEstimateConfig);
        setEstimatedSize(
            EstimateSpec.getChildSizeWithMode(width, width, EstimateSpec.NOT_EXCEED),
            EstimateSpec.getChildSizeWithMode(height, height, EstimateSpec.NOT_EXCEED));
        return true;
    }
}