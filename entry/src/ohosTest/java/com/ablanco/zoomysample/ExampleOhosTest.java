package com.ablanco.zoomysample;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
/**
 * 自动化测试类
 *
 * @since 2021-07-14
 */

public class ExampleOhosTest {
    /**
     * 测试用例(UI控件不涉及单元测试)
     *
     */
    @Test
    public void testBundleName() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        assertEquals("com.ablanco.zoomysample", actualBundleName);
    }
}