package com.ablanco.zoomy;

import ohos.agp.components.Component;

/**
 * Created by Álvaro Blanco Cabrero on 13/02/2017.
 * Zoomy.
 */
public interface TapListener {
    void onTap(Component component);
}
