package com.ablanco.zoomy;

import ohos.agp.components.Component;

/**
 * Created by Álvaro Blanco Cabrero on 12/02/2017.
 * Zoomy.
 */
public interface ZoomListener {
    /**
     * 动画开始事件
     *
     * @param component component
     */
    void onViewStartedZooming(Component component);

    /**
     * 动画结束事件
     *
     * @param component component
     */
    void onViewEndedZooming(Component component);
}
