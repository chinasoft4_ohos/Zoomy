/*
 * Copyright (C) 2021 Huawei Device Co., Ltd. 2012-2020. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.ablanco.zoomy;

import ohos.agp.components.Component;
import ohos.multimodalinput.event.MmiPoint;
import ohos.multimodalinput.event.TouchEvent;

/**
 *
 * Compat
 *
 * @since 2021-07-14
 */
public class Compat {

    /**
     * getTouchX
     *
     * @param touchEvent touchEvent
     * @param index index
     * @param component component
     * @return float
     */
    public static float getTouchX(TouchEvent touchEvent, int index, Component component) {
        float touchEventX = 0;
        if (touchEvent.getPointerCount() > index) {
            int[] xy = component.getLocationOnScreen();
            if (xy != null && xy.length == 2) {
                touchEventX = touchEvent.getPointerScreenPosition(index).getX() - xy[0];
            } else {
                touchEventX = touchEvent.getPointerPosition(index).getX();
            }
        }
        return touchEventX;
    }

    /**
     * getTouchY
     *
     * @param touchEvent touchEvent
     * @param index index
     * @param component component
     * @return float
     */
    public static float getTouchY(TouchEvent touchEvent, int index, Component component) {
        float touchEventY = 0;
        if (touchEvent.getPointerCount() > index) {
            int[] xy = component.getLocationOnScreen();
            if (xy != null && xy.length == 2) {
                touchEventY = touchEvent.getPointerScreenPosition(index).getY() - xy[1];
            } else {
                touchEventY = touchEvent.getPointerPosition(index).getY();
            }
        }
        return touchEventY;
    }

    /**
     * getTouchPoint
     *
     * @param touchEvent touchEvent
     * @param index index
     * @param component component
     * @return MmiPoint
     */
    public static MmiPoint getTouchPoint(TouchEvent touchEvent, int index, Component component) {
        float touchEventX = 0;
        float touchEventY = 0;
        if (touchEvent.getPointerCount() > index) {
            int[] xy = component.getLocationOnScreen();
            if (xy != null && xy.length == 2) {
                touchEventX = touchEvent.getPointerScreenPosition(index).getX() - xy[0];
            } else {
                touchEventX = touchEvent.getPointerPosition(index).getX();
            }
        }
        if (touchEvent.getPointerCount() > index) {
            int[] xy = component.getLocationOnScreen();
            if (xy != null && xy.length == 2) {
                touchEventY = touchEvent.getPointerScreenPosition(index).getY() - xy[1];
            } else {
                touchEventY = touchEvent.getPointerPosition(index).getY();
            }
        }
        return new MmiPoint(touchEventX, touchEventY);
    }
}
