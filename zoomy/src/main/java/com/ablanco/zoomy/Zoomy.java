package com.ablanco.zoomy;

import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.Image;

/**
 * Created by Álvaro Blanco Cabrero on 12/02/2017.
 * Zoomy.
 */
public class Zoomy {

    private static ZoomyConfig mDefaultConfig = new ZoomyConfig();

    private Zoomy() {
    }

    /**
     * 设置自定义config
     *
     * @param config 配置config
     */
    public static void setDefaultConfig(ZoomyConfig config) {
        mDefaultConfig = config;
    }

    /**
     * 注销
     *
     * @param component component
     */
    public static void unregister(Component component) {
        component.setTouchEventListener(null);
    }


    /**
     * Zoomy Builder
     */
    public static class Builder {

        private boolean mDisposed = false;

        private ZoomyConfig mConfig;
        private ComponentContainer mTargetContainer;
        private Image mTargetView;
        private ZoomListener mZoomListener;

        private TapListener mTapListener;
        private LongPressListener mLongPressListener;
        private DoubleTapListener mdDoubleTapListener;
        private int mCurveType;


        /**
         * Builder构建方法
         *
         * @param componentContainer 主视图
         */
        public Builder(ComponentContainer componentContainer) {
            this.mTargetContainer = componentContainer;
        }

        /**
         * 动画效果type
         *
         * @param interpolator CurveType
         * @return Builder
         */
        public Builder interpolator(int interpolator) {
            checkNotDisposed();
            this.mCurveType = interpolator;
            return this;
        }

        /**
         * 顶部缩放视图
         *
         * @param target target
         * @return Builder
         */
        public Builder target(Image target) {
            this.mTargetView = target;
            return this;
        }

        /**
         * 动画设置
         *
         * @param animate 是否开启动画
         * @return Builder
         */
        public Builder animateZooming(boolean animate) {
            checkNotDisposed();
            if (mConfig == null) {
                mConfig = new ZoomyConfig();
            }
            this.mConfig.setZoomAnimationEnabled(animate);
            return this;
        }

        /**
         * 沉浸模式设置
         *
         * @param enable 是否开启沉浸模式
         * @return Builder
         */
        public Builder enableImmersiveMode(boolean enable) {
            checkNotDisposed();
            if (mConfig == null) {
                mConfig = new ZoomyConfig();
            }
            this.mConfig.setImmersiveModeEnabled(enable);
            return this;
        }

        /**
         * 自定义动画监听
         *
         * @param listener zoomListener
         * @return Builder
         */
        public Builder zoomListener(ZoomListener listener) {
            checkNotDisposed();
            this.mZoomListener = listener;
            return this;
        }


        /**
         * 单击监听
         *
         * @param listener TapListener
         * @return Builder
         */
        public Builder tapListener(TapListener listener) {
            checkNotDisposed();
            this.mTapListener = listener;
            return this;
        }

        /**
         * 长按监听
         *
         * @param listener LongPressListener
         * @return Builder
         */
        public Builder longPressListener(LongPressListener listener) {
            checkNotDisposed();
            this.mLongPressListener = listener;
            return this;
        }

        /**
         * 双击监听
         *
         * @param listener doubleTapListener
         * @return Builder
         */
        public Builder doubleTapListener(DoubleTapListener listener) {
            checkNotDisposed();
            this.mdDoubleTapListener = listener;
            return this;
        }

        /**
         * 注册
         */
        public void register() {
            checkNotDisposed();
            if (mConfig == null) {
                mConfig = mDefaultConfig;
            }
            if (mTargetContainer == null) {
                throw new IllegalArgumentException("Target container must not be null");
            }
            if (mTargetView == null) {
                throw new IllegalArgumentException("Target component must not be null");
            }
            mTargetView.setTouchEventListener(new ZoomableTouchListener(mTargetContainer, mTargetView,
                mConfig, mZoomListener, mCurveType, mTapListener, mLongPressListener, mdDoubleTapListener));

            mDisposed = true;
        }

        /**
         * 检测mDisposed
         */
        private void checkNotDisposed() {
            if (mDisposed) {
                throw new IllegalStateException("Builder already disposed");
            }
        }
    }
}
