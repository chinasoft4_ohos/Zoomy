package com.ablanco.zoomy;

import ohos.agp.components.Component;

/**
 * Created by Álvaro Blanco Cabrero on 04/08/2018.
 * Zoomy.
 */
public interface LongPressListener {
    void onLongPress(Component component);
}
