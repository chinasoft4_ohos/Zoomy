package com.ablanco.zoomy;

import ohos.agp.utils.Point;
import ohos.multimodalinput.event.TouchEvent;

/**
 * Created by Álvaro Blanco Cabrero on 11/02/2017.
 * Zoomy.
 */
public class MotionUtils {

    static void midPointOfEvent(Point point, TouchEvent event) {
        if (event.getPointerCount() == 2) {
            float pointX = event.getPointerPosition(0).getX() + event.getPointerPosition(1).getX();
            float pointY = event.getPointerPosition(0).getY() + event.getPointerPosition(1).getY();
            point.modify(pointX / 2, pointY / 2);
        }
    }
}
