package com.ablanco.zoomy;

import ohos.agp.components.Component;
import ohos.agp.utils.Point;

/**
 * Created by Álvaro Blanco Cabrero on 11/02/2017.
 * Zoomy.
 */
public class ViewUtils {
    static Point getViewAbsoluteCords(Component component) {
        int[] location = component.getLocationOnScreen();

        int locationX = location[0];
        int locationY = location[1];

        return new Point(locationX, locationY);
    }
}
