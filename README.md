# Zoomy


#### 项目介绍
- 项目名称：Zoomy
- 所属系列：openharmony的第三方组件适配移植
- 功能：一个使用方便的捏合缩放库
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio 2.2 Beta2
- 基线版本： master分支

#### 效果演示
<img src="img/demo.gif"></img>

#### 安装教程
                                
1.在项目根目录下的build.gradle文件中，
 ```gradle
allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/releases/'
        }
    }
}
 ```
2.在entry模块的build.gradle文件中，
 ```gradle
 dependencies {
    implementation('com.gitee.chinasoft_ohos:Zoomy:1.0.0')
    ......  
 }
```

在sdk6，DevEco Studio2.2 beta2下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明

要开始使用 Zoomy，只需注册您想要缩放的视图

```java

Zoomy.Builder builder = new Zoomy.Builder(componentContainer).target(mZoomableView);
builder.register();

```
就这样。现在您的视图可以缩放了！
Zoomy 也可以取消注册视图

```java
Zoomy.unregister(mZoomableView);

```
#### 自定义

Zoomy 允许一些自定义配置:

+ 使用 ZoomyConfig 更改默认配置标志

```java
ZoomyConfig config = new ZoomyConfig();
config.setZoomAnimationEnabled(false); //释放视图时启用缩小动画（默认为 true）
config.setImmersiveModeEnabled(false); //缩放视图时允许进入沉浸模式（默认为 true）       
```

+ 现在将其设置为所有 Zoomy 注册视图的默认配置
```java
Zoomy.setDefaultConfig(config);           
```

Zoomy builder 也允许一些自定义：

+ Zoomy config flags 也可以在构建Zoomy时设置。
config flags将覆盖默认的 ZoomyConfig 标志。
```java
    Zoomy.Builder builder = new Zoomy.Builder(this)
                    .target(mZoomableView)
                    .enableImmersiveMode(false)
                    .animateZooming(false);
```

+ 您可以添加回调以侦听特定事件。因为Zoomy 是在注册的Component 上附加一个Component.TouchEventListener 来工作的，所以Component.OnClickListener 不能和Zoomy 一起设置，
所以提供了TapListener、LongPressListener 和DoubleTapListener 来保证Component 仍然可以监听手势。如果您对缩放事件感兴趣，还提供 ZoomListener。
```java
 Zoomy.Builder builder = new Zoomy.Builder(this)
                    .target(mZoomableView)
                    .tapListener(new TapListener() {
                        @Override
                        public void onTap(Component c) {
                            //component tapped, do stuff
                        }
                    })
                     .longPressListener(new LongPressListener() {
                        @Override
                        public void onLongPress(Component c) {
                            //component long pressed, do stuff
                        }
                    }).doubleTapListener(new DoubleTapListener() {
                        @Override
                        public void onDoubleTap(Component c) {
                            //component double tapped, do stuff
                        }
                    })
                    .zoomListener(new ZoomListener() {
                        @Override
                        public void onViewStartedZooming(Component c) {
                            //component started zooming
                        }

                        @Override
                        public void onViewEndedZooming(Component c) {
                            //component ended zooming
                        }
                    });        
```

+ 可以更改动画结束缩放事件时使用动画效果。

```java
   Zoomy.Builder builder = new Zoomy.Builder(this)
                    .target(mZoomableView)
                    .interpolator(Animator.CurveType.OVERSHOOT);
```

#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异


#### 版本迭代

- 1.0.0

#### 版权和许可信息

    Copyright 2017 Álvaro Blanco Cabrero
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
